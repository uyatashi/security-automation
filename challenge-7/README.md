# Challenge 7

This challenge describes how to run our SAST OWASP dependency check scan

----


### Pull the Bandit container

##### command:

```bash
docker pull owasp/dependency-check:latest
```

#### output:

```bash
Using default tag: latest
latest: Pulling from owasp/dependency-check:latest
cbdbe7a5bc2a: Downloading [=================>                                 ]    961kB/2.813MB
26ebcd19a4e3: Download complete 
8341bd19193b: Downloading [==>                                                ]  846.3kB/20.69MB
ecc595bd65e1: Waiting 
4b1c9d8f69d2: Waiting 
4acb96206c62: Waiting 
88da57106cb7: Waiting 
......... snip ........
```

----

### Change your directory to the flask-app dir

##### command:

```bash
cd flask-app
```
--

### Start our OWASP dependency check scanner

##### command:

Note that $(pwd) is only supported on Linux and MacOS - on Windows you will need to replace this with the full current working directory.

```bash
docker run --rm \
    --volume $(pwd):/src:z \
    --volume "$DATA_DIRECTORY":/usr/share/dependency-check/data:z \
    --volume $(pwd)/odc-reports:/report:z \
    owasp/dependency-check \
    --scan /src \
    --format "ALL" \
    --project test \
    --out /report
```

##### output:

```bash
[INFO] Checking for updates
[INFO] NVD CVE requires several updates; this could take a couple of minutes.
[INFO] Download Started for NVD CVE - 2003
[INFO] Download Started for NVD CVE - 2002
[INFO] Download Complete for NVD CVE - 2003  (1326 ms)
[INFO] Processing Started for NVD CVE - 2003
[INFO] Download Started for NVD CVE - 2004
......... snip ..........
```

For the complete scan output check the "odc-reports" folder.

See if you can optimize scan results by digging into the docs!

----