# Challenge 8

----

Firstly in Gitlab find the "packages & registries" tab
In your container registry you can find the following commands:

##### command:

```bash 
docker login registry.gitlab.com -u <username> -p <token>
docker build -t registry.gitlab.com/riiecco1/security-automation/test-zap:1.0 .
docker push registry.gitlab.com/riiecco1/security-automation/test-zap:1.0
```

Note: before you can push an image that was not build by yourself
to your registry you have to re-tag the image you want to push as following:

```bash
docker tag owasp/zap2docker-stable registry.gitlab.com/riiecco1/security-automation/test-zap:1.0
```

